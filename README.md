# Color Picker Demo #

This is a small bit of sample code demonstrating a single page app that allows you to view colors based on RGB values you set with sliders.

## Page Structure ##

The page uses only StackLayouts to organize all visual elements. Some have a vertical orientation, some horizontal. The rest is very simple:  A BoxView to show the color selection, 3 sliders to set red green and blue values, and labels next to each slider.

## Functionality ##
* OnPageAppearing handles the "Appearing" event of the ContentPage. This sets the slider values, and the color for the BoxView.
* OnColorSliderChanged handles the changes for *all 3* sliders. We do not need to know which slider the user has changed... Just that the RGB values need to be used to create a new color.
* Xamarin.Forms colors use RGB values from 0 - 1 rather than 0 - 255, so we need to convert our slider values into values useable by Xamarin.Forms.


## Screenshots of Both Platforms
![Screenshot of this sample app running on both Android and iOS](ColorPickerDemoBothPlatforms.png "Color Picker Sample")

## Questions?

* Always always always: If you have a question, someone else in class almost certainly has that same question, so please do ask: [Post questions on Cougar Courses](https://cc.csusm.edu/mod/oublog/view.php?id=630348)